package logic;


import customer.Transaction;
import mumbai.common.UserID;
import customer.Customer;


public interface CustomerHandler {

	boolean existCustomerByCPR(String cpr);

	void createCustomer(String name, String cpr);

	void removeCustomerByCPR(String cpr);


	Transaction getTransactionByToken(String token);

	Customer getCustomerByCPR(UserID key);
	
	void addNewTransaction(Transaction transaction);
}
