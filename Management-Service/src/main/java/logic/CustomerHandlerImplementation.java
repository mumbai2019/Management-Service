package logic;

import java.math.BigDecimal;
import java.util.TreeSet;

import customer.*;
import database.*;
import mumbai.common.UserID;
import mumbai.common.UserIDImplementation;

/**
 * @author s164168 Adam
 */


public class CustomerHandlerImplementation implements CustomerHandler
{

	Database database;
	
	//Setup the database on creation
	public CustomerHandlerImplementation(){
		database = new InMemoryDB();
	}
	
	
	//Checks if the customere is in our hashmap
	@Override
	public boolean existCustomerByCPR(String cpr){

		//Translates the cpr into a cpr implementation
		UserID key = new UserIDImplementation(cpr);
		
		//Looks for the given cpr in the hashmap
		return database.getConnection().containsKey(key);
		
	}


	/**
	 * Adds the user to the database
	 * @author s164168 Adam
	 * @throws IllegalArgumentException
	 */
	@Override
	public void createCustomer(String name, String cpr) {
		//Checks for duplicate users
		if (existCustomerByCPR(cpr)){
			throw new IllegalArgumentException("User already exists");
		}
		//Makes the customer and adds it to the database
		Customer customer = new CustomerImplementation(name, cpr);
		database.getConnection().put(customer.getUserID(), customer);
		
	}

	/**
	 * Removes the customer if it exists
	 * @author s164168 Adam
	 * @throws IllegalArgumentException
	 */
	@Override
	public void removeCustomerByCPR(String cpr) {
		
		UserID key = new UserIDImplementation(cpr);
		if(!database.getConnection().containsKey(key)){
			throw new IllegalArgumentException("User doesn't exist");
		}
		database.getConnection().remove(key);
	}


	
	@Override
	public Customer getCustomerByCPR(UserID key) {
		
		if(!database.getConnection().containsKey(key)){
			throw new IllegalArgumentException("User doesn't exist");
		}
		return  database.getConnection().get(key);
	}

	public Transaction getTransactionByToken(String token) {
		if(!database.getTransaction().containsKey(token)){
			throw new IllegalArgumentException("Token doesn't exist");
		}
		return database.getTransaction().get(token);
		
		
	}


	@Override
	public void addNewTransaction(Transaction transaction) {
		database.getTransaction().put(transaction.getSenderToken(), transaction);
	}
	
}
