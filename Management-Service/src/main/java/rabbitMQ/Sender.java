package rabbitMQ;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

public interface Sender {
	
	void pushMessageTo(String message, String routingKey) throws UnsupportedEncodingException, IOException, TimeoutException;

}
