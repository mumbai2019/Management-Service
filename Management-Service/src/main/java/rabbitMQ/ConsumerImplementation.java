package rabbitMQ;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import logic.*;

/**
 *  @author s164168 Adam
 */

public class ConsumerImplementation implements Consumer
{
	
	CustomerHandler handler;
	SenderImplementation sender = new SenderImplementation();
	
	

	public ConsumerImplementation(CustomerHandler handler2) {
		handler = handler2;
	}



	@Override
	public void consumeMessage(String consumerTag, String message) throws IOException, TimeoutException {
		System.out.println("recieved: " + message);
		JSONObject jsonInput = new JSONObject(message);
		
		EventEnums enumEvent = EventEnums.valueOf(jsonInput.getString("event"));
		enumEvent.handleEvent(handler, jsonInput, sender);

			
	}
	
	
}