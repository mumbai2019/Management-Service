package rabbitMQ;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;


/**
 * @author s164168
 * Receives messages from RabbitMQ and handles them asynchronously.
 * Mocks the payout service, so tests can be run
 */
public class ReceiverTester extends Thread {
	private final static String EXCHANGE_NAME = "serviceExchange";
	String lastMsg= "";
	Boolean msgRcvd;
	
	public synchronized String lastMsg(int timeout) throws TimeoutException, InterruptedException, ExecutionException{
		if(!msgRcvd){
			wait(timeout);
		}
		
		return lastMsg;
	}
	
	public void run() {
		try {
			
			System.out.println("Receiver says HELLO!");
			
			ConsumerImplementation consume = new ConsumerImplementation(null);
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("rabbitmq");
			
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			
			channel.exchangeDeclare(EXCHANGE_NAME, "direct");
			String queueName = channel.queueDeclare().getQueue();
			
			
			//Routing keys to listen for.
			
			channel.queueBind(queueName, EXCHANGE_NAME, "payout");
			
			
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
				System.out.println("Recived message: " + new String(delivery.getBody(), "UTF-8"));
				 lastMsg = new String(delivery.getBody(), "UTF-8");
				 msgRcvd = true;
				 notify();
		    };
		    System.out.println("Receiver says HELLO!");
		    channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
		    
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
}
