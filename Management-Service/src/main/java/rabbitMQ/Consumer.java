package rabbitMQ;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;

public interface Consumer {
	
	void consumeMessage(String consumerTag, String message) throws UnsupportedEncodingException, IOException, TimeoutException;

}
