package rabbitMQ;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.TreeSet;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;

import customer.Customer;
import customer.Transaction;
import customer.TransactionImplementation;
import logic.CustomerHandler;
import mumbai.common.UserID;
import mumbai.common.UserIDImplementation;

/**
 * EventEnums handler.
 * This handles the messages recived from rabbitmq depending on it's Enum 
 * @author s164168 Adam
 *
 */

public enum EventEnums 
{
    TOKENUSEEVENT
    {
    	//We have no use for this token
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) {}
    },
    TOKENAUTHEVENT{
    	//We have no use for this token
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) {}	
    },
    REFUNDVERIFYEVENT{
    	/**
    	 * @author s164161 Emil
    	 * 
    	 * */
    	@Override
    	public void handleEvent(CustomerHandler handler,JSONObject message, Sender sender) {
    		
    		
    		Transaction t = handler.getTransactionByToken(message.get("token").toString());
    		JSONObject response = new JSONObject();
    		if(!t.isRefunded()) {
    			response.put("event",EventEnums.REFUNDAUTHEVENT);
    			response.put("response", true);
        		response.put("customer", t.getSenderCPR().getID());
        		response.put("amount",t.getMoneySend());
        		try {
					sender.pushMessageTo(response.toString(), "payout");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			
    		}
    		else {
    			response.put("event",EventEnums.REFUNDAUTHEVENT);
    			response.put("reponse", false);
        		try {
					sender.pushMessageTo(response.toString(), "payout");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			
    		}
    		
    		//Recivcves, Merchant + Token
    		//Reponse: "response":bool, "customer":s, "amount":BigDecilmal
    		/**
    		 * Get merchant transaction history, find token,
    		 * If not been refunded before
    		 * Response true
    		 * else 
    		 * False
    		 * 
    		 * 
    		 * */
    		
    	}
    },REFUNDDONE{
    	public void handleEvent(CustomerHandler handler,JSONObject message, Sender sender){
    		handler.getTransactionByToken(message.get("token").toString()).refund();
    	
    }},
    	
    USERVERIFYEVENT{
    	/**
    	 * We are to check if the given cpr matches a user in our database
    	 * @author s164161 Emil
    	 * @throws UnsupportedEncodingException
    	 * @throws IOException
    	 * @throws TimeoutException
    	 */
		@Override
		public void handleEvent(CustomerHandler handler, JSONObject jsonInput, Sender sender) throws UnsupportedEncodingException, IOException, TimeoutException {
		
			//We extract the string from the JSONObject
			//Then we call the handler, and ask it to check if the given CPR exists
			Boolean response = handler.existCustomerByCPR(jsonInput.getString("cpr"));
			
			//We now create a JSONObject to send back to payout
			JSONObject outgoingJson = new JSONObject();
			outgoingJson.put("event", USERAUTHEVENT);
			outgoingJson.put("response", response);
			
			//Sends the message to payout, using the sender
			sender.pushMessageTo(outgoingJson.toString(), "payout");			
		}
    	
    },
    USERAUTHEVENT{
    	//We have no use for this token
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) {
		}
    	
    },
    REPORTINFOPROVIDE{
    	//
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject jsonInput, Sender sender) throws UnsupportedEncodingException, IOException, TimeoutException {
    	//Makes the id from the userID	
    	UserID cpr = new UserIDImplementation(jsonInput.getString("cpr"));
    	//Finds the customer
    	Customer customer = handler.getCustomerByCPR(cpr);
    	
    	//Gets all transaction to and from user
    	TreeSet<Transaction> transactionRecv = customer.getRecieveTransaction();
    	TreeSet<Transaction> transactionSend = customer.getSendTransaction();
    	
    	//Creates response jsonobject and adds the event enum
    	JSONObject jsonOut= new JSONObject();
    	jsonOut.put("event", EventEnums.REPORTINFO);
    	
    	//Adds the token, name, money send and refund status to the jObject, for all transactions including the customer
    	int i = 0;
    	for(Transaction transaction : transactionRecv){
    		jsonOut.put("token" + i, transaction.getSenderToken());
    		Customer name = handler.getCustomerByCPR(transaction.getReceiverCPR());
    		jsonOut.put("name" + i, name.getUserName());
    		jsonOut.put("money" + i, transaction.getMoneySend());
    		jsonOut.put("refunded" + i, transaction.isRefunded());
    		i++;
    	}
    	for(Transaction transaction : transactionSend){
    		jsonOut.put("token" + i, transaction.getSenderToken());
    		Customer name = handler.getCustomerByCPR(transaction.getReceiverCPR());
    		jsonOut.put("name" + i, name.getUserName());
    		jsonOut.put("money" + i, transaction.getMoneySend());
    		jsonOut.put("refunded" + i, transaction.isRefunded());
    		i++;
    	}
    	
    	//Sends the message
    	sender.pushMessageTo(jsonOut.toString(), "report");
    	
    	}
    	
    },
    REPORTINFO{
    	//We have no use for this token
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) {
		}
    	
    },
    NEWTRANSACTION{
    	//
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject jsonInput, Sender sender) {
    		//Extracts information
    		String token = jsonInput.getString("token");
    		BigDecimal amount = jsonInput.getBigDecimal("amount");
			String date = jsonInput.getString("date");
    	
			//Extracts cpr and makes it into a ID
			UserID merchantCpr = new UserIDImplementation(jsonInput.getString("merchantCpr"));
    		UserID customerCpr = new UserIDImplementation(jsonInput.getString("customerCpr"));
    	
    		//Finds the customers from ID
    		Customer merchant = handler.getCustomerByCPR(merchantCpr);
    		Customer customer = handler.getCustomerByCPR(customerCpr);
    		
    		//Makes the transaction with given information
    		Transaction transaction = new TransactionImplementation(token, merchantCpr, customerCpr, amount, date);
    		
    		//Distributes the transaction to the needed locations
    		handler.addNewTransaction(transaction);
    		merchant.recieveTransaction(transaction);
    		customer.sendTransaction(transaction);
    		
		}
    	
    },
    REFUNDAUTHEVENT{
    	//We havno use for this token
    	@Override
    	public void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) {
		}
    };
	
	public abstract void handleEvent(CustomerHandler handler, JSONObject message, Sender sender) throws UnsupportedEncodingException, IOException, TimeoutException;
}
