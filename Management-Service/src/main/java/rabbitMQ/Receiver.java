package rabbitMQ;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import logic.CustomerHandler;

import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.Channel;


/**
 * @author s160902
 * @author s164168 Modified to be used with managment service
 * Receives messages from RabbitMQ and handles them asynchronously.
 */
public class Receiver extends Thread {
	private final static String EXCHANGE_NAME = "serviceExchange";
	ConsumerImplementation consume;
	
	public Receiver(CustomerHandler handler) {
		consume =  new ConsumerImplementation(handler);
	}

	public void run() {
		try {
			
			
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("rabbitmq");
			
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			
			channel.exchangeDeclare(EXCHANGE_NAME, "direct");
			String queueName = channel.queueDeclare().getQueue();
			
			
			//Routing keys to listen for.
			
			channel.queueBind(queueName, EXCHANGE_NAME, "manager");
			
			
			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
				System.out.println("Message recived: " + new String(delivery.getBody(), "UTF-8"));
				try {
					consume.consumeMessage(consumerTag, new String(delivery.getBody(), "UTF-8"));
				} catch (TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		    };
		    System.out.println("Receiver says HELLO!");
		    channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
		    
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
	}

	
	
	
	
	
}
