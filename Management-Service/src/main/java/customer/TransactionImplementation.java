package customer;

import java.math.BigDecimal;
import mumbai.common.UserID;

/**
 * Implementation of the Transactions.
 * @author s164161 Emil
 */

public class TransactionImplementation implements Transaction
{

	String date;
	String senderToken;
	UserID receiverCPR;
	UserID senderCPR;
	BigDecimal moneySend;
	Boolean refunded = false;
	
	
	public TransactionImplementation(String senderToken, UserID CPRReceiver, UserID CPRSender, BigDecimal moneySend, String date){
		this.date = date;		
		this.senderToken = senderToken;
		this.receiverCPR = CPRReceiver;
		this.senderCPR = CPRSender;
		this.moneySend = moneySend;
	}
	
	@Override
	public String getDate() {
		return date;
	}

	@Override
	public UserID getSenderCPR() {
		return senderCPR;
	}

	@Override
	public UserID getReceiverCPR() {
		return receiverCPR;
	}

	@Override
	public String getSenderToken() {
		return senderToken;
	}

	@Override
	public BigDecimal getMoneySend() {
		return moneySend;
	}
	
	@Override
	public boolean isRefunded() {
		return refunded;
	}
	
	@Override
	public void refund() {
		refunded = true;
	}
	
	
	@Override
	public int compareTo(Transaction other) {
		return date.compareTo(other.getDate());
	}
	
}