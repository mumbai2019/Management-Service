package customer;

import java.util.Comparator;

public class ComparatorCustomerImplementation implements Comparator<Transaction>{

	@Override
	public int compare(Transaction r1, Transaction r2) {
		return r1.compareTo(r2);
	}
	
}