package customer;

import java.util.TreeSet;

import mumbai.common.UserID;

public interface Customer
{
	public String getUserName();
	public int getIDhashcode();
	public UserID getUserID();
	void recieveTransaction(Transaction transaction);
	void sendTransaction(Transaction transaction);
	TreeSet<Transaction> getRecieveTransaction();
	TreeSet<Transaction> getSendTransaction();
}
