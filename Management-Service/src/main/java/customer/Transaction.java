package customer;

import java.math.BigDecimal;
import mumbai.common.UserID;

/**
 * Interface for the Transactions
 * @author s164161 emil
 *
 */


public interface Transaction
{
	public String getDate();
	public UserID getSenderCPR();
	public UserID getReceiverCPR();
	public String getSenderToken();
	public BigDecimal getMoneySend();
	int compareTo(Transaction other);
	boolean isRefunded();
	void refund();
	
	
}