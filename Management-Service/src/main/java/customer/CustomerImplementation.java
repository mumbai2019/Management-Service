package customer;



import java.util.Comparator;
import java.util.TreeSet;

import mumbai.common.UserID;
import mumbai.common.UserIDImplementation;

public class CustomerImplementation implements Customer
{
	private UserID cpr;
	private String name;
	
	TreeSet<Transaction> recievedTransactions;
	TreeSet<Transaction> sendTransactions;
	Comparator<Transaction> compareTransaction = new ComparatorCustomerImplementation();
	
	/**
	 * Creates a user with a name and a CPR number which is transform into a value object
	 * @param name
	 * @param cpr
	 */
	public CustomerImplementation(String name, String cpr) {
		this.cpr = new UserIDImplementation(cpr);
		this.name = name;
		recievedTransactions = new TreeSet<Transaction>(compareTransaction);
		sendTransactions 	 = new TreeSet<Transaction>(compareTransaction);
	}
	
	public String getUserName() {
		return name;
	}
	
	public int getIDhashcode() {
		return  cpr.hashCode();
	}
	
	public UserID getUserID() {
		return cpr;
	}
	
	@Override
	public void recieveTransaction(Transaction transaction){
		recievedTransactions.add(transaction);
	}
	
	@Override
	public void sendTransaction(Transaction transaction){
		sendTransactions.add(transaction);
	}
	
	@Override
	public TreeSet<Transaction> getRecieveTransaction(){
		return recievedTransactions;
	}
	
	@Override
	public TreeSet<Transaction> getSendTransaction(){
		return sendTransactions;
	}
	
	
}
