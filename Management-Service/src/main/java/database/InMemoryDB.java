package database;

import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeSet;

//Only here for hardcoded users TODO
import customer.*;
import mumbai.common.UserID;
import mumbai.common.UserIDImplementation;

/**
 * @author s164168 Adam
 */


public class InMemoryDB implements Database
{
	private HashMap<UserID, Customer> hashmap;
	private HashMap<String, Transaction> transactions = new HashMap<String, Transaction>();
	
	public InMemoryDB() {
		//Creates the HashMap of Customers, sorted by their CPR number
		hashmap = new HashMap<UserID, Customer>();
		//Adds hard coded user to the database
		UserIDImplementation tempCPR = new UserIDImplementation("251287-0073");
		hashmap.put(tempCPR, new CustomerImplementation("Emil Kruse", "251287-0073"));
		
		tempCPR = new UserIDImplementation("201397-0071");
		hashmap.put(tempCPR, new CustomerImplementation("Adam Herreborg", "201397-0071"));
		
		tempCPR = new UserIDImplementation("mumbaiTestOne");
		hashmap.put(tempCPR, new CustomerImplementation("Adam Herreborg", "mumbaiTestOne"));
		
		tempCPR = new UserIDImplementation("mumbaiTestTwo");
		hashmap.put(tempCPR, new CustomerImplementation("Adam Herreborg", "mumbaiTestTwo"));

	}

	//Allows the system to get the hashmap
	public HashMap<UserID, Customer> getConnection() 
	{
		return hashmap;
	}

	@Override
	public HashMap<String, Transaction> getTransaction() {
		return transactions;
	}
}
