package database;

import java.util.HashMap;
import java.util.TreeSet;


import customer.Customer;
import customer.Transaction;
import mumbai.common.UserID;

public interface Database {
	
	public HashMap<UserID, Customer> getConnection();

	HashMap<String, Transaction> getTransaction();

}
