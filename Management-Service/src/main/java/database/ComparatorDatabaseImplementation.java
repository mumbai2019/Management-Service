package database;

import java.util.Comparator;

import customer.Transaction;

public class ComparatorDatabaseImplementation implements Comparator<Transaction>{

	

	@Override
	public int compare(Transaction t1, Transaction t2) {
		
		return t1.getSenderToken().compareTo(t2.getSenderToken());
	}
	
}