package rest;

import javax.ws.rs.core.Application;

import javax.ws.rs.ApplicationPath;
/**
 * 
 * @author s164168 Modified to work with management service
 *
 */
@ApplicationPath("/") //Don't touch this
public class RestApplication extends Application {
	public static Factory microservice = new Factory();
	public RestApplication(){
		microservice.runReceiver();
		
	}
}
