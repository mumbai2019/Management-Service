package rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import logic.CustomerHandler;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
/**
 * Endpoint, that handles rest calls
 * @author s164161 Emil
 * */
@Path("/manager")
public class ManagerEndpoint {
	
	  @GET
	  @Produces("text/plain")
	  public Response doGet() {
	    return Response.ok("Welcome to the manager rest-interface").build();
	  }
	  
	   
	  
	  @POST
	  @Path("/signup")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response createUser(String inputJSONObj){
		  //Return message
		  JSONObject returnJson = new JSONObject();
		  try {
			  //Gets the recived message
			  JSONObject json = new JSONObject(inputJSONObj);
			  //Grabs the handler so we can interact with the database 
			  CustomerHandler handler = RestApplication.microservice.getHandler();
			  
			  //Extracts cpr and name. NB name consists of first name plus last name with a space in between
			  String cpr = json.getString("cpr");
			  String name = json.getString("firstName")+ " " + json.getString("lastName");
			  
			  //If the user dosen't already exist, we create it.
			  if(!handler.existCustomerByCPR(cpr)){
				  
				  handler.createCustomer(name, cpr);
				 
				  //We respond with a success
				  return Response
						  .status(Response.Status.OK)
						  .entity(returnJson.toString())
						  .type(MediaType.APPLICATION_JSON)
						  .build();
						  
			  }
			  
			  //If the user existed we respond with a error and conflict
			  returnJson.put("error", "User Exists");
			  return Response
					  .status(Response. Status.CONFLICT)
					  .entity(returnJson.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		} catch (Exception e) {
			//If a error is encountered we respond with the exception
			returnJson.put("error", "Exception thrown: " + e.getMessage());
			  return Response
					  .status(Response.Status.INTERNAL_SERVER_ERROR)
					  .entity(returnJson.toString())
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		}
				  
		  
	  }
	  
}

