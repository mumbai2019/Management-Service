package rest;

import logic.CustomerHandler;
import logic.CustomerHandlerImplementation;
import rabbitMQ.Receiver;

public class Factory {

	/**
	 * Factory to set up the different parts of the systems and sort out dependecies
	 * @author s164168 Adam
	 */
	CustomerHandler handler = new CustomerHandlerImplementation();
	public CustomerHandler getHandler(){
		return handler;
	}

	
	public void runReceiver() {
		System.out.print("I'm actually printing!!!!");
		Receiver receiver = new Receiver(handler);
		receiver.run();
	}

	
}
