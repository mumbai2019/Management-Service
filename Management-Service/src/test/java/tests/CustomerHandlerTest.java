package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import logic.*;


/**
 * Made to testing CustomerHanlder.
 * @author s164168 Adam
 */
public class CustomerHandlerTest
{
	CustomerHandler handler;
	/**
	 * @author s164168 Adam
	 */
	@Before
	public void init()
	{
		handler = new CustomerHandlerImplementation();		
		//handler.createCustomer("Emil Kruse", "251287-0073");
		
	}
	 
	/**
	 * @author s164168 Adam
	 */
	@Test
	public void findUserByCPR()
	{
		assertTrue(handler.existCustomerByCPR("251287-0073"));
	}
	
	/**
	 * @author s164168 Adam
	 */
	@Test
	public void failFindUserByCPR()
	{
		assertFalse(handler.existCustomerByCPR("cpr"));
	}
	
	/** 
	 * Tests the functionality of adding user
	 * @author s164168 Adam
	 */
	@Test
	public void addUser()
	{
		String name = "Tester mc testface";
		String cpr = "123456-7890";
		//Checks that we don't have the user
		assertFalse(handler.existCustomerByCPR(cpr));
		
		handler.createCustomer(name, cpr);
		
		//Checks that we the user is know there		
		assertTrue(handler.existCustomerByCPR(cpr));
		
		
	}

	/** 
	 * Tests the functionality of adding user, when user is there
	 * @author s164168 Adam
	 */
	@Test
	public void addDuplicateUser()
	{
		String name = "Emil Kruse";
		String cpr = "251287-0073";
		//Checks that we don't have the user
		assertTrue(handler.existCustomerByCPR(cpr));
		
		try {
			handler.createCustomer(name, cpr);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("User already exists", e.getMessage());
		}
		
		
		
	}
	
	/** 
	 * Tests the functionality of removing a user
	 * @author s164168 Adam
	 */
	@Test
	public void removeUser()
	{
		String cpr = "251287-0073";
		assertTrue(handler.existCustomerByCPR(cpr));
		
		handler.removeCustomerByCPR(cpr);
		
		assertFalse(handler.existCustomerByCPR(cpr));
		
	}

	/** 
	 * Tests the functionality of removing a user
	 * @author s164168 Adam
	 */
	@Test
	public void removeUserNotExsisting()
	{
		String cpr = "123456-7890";
		assertFalse(handler.existCustomerByCPR(cpr));
		
		try {
			handler.removeCustomerByCPR(cpr);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("User doesn't exist", e.getMessage());
		}
		
		assertFalse(handler.existCustomerByCPR(cpr));
		
	}
	
}
