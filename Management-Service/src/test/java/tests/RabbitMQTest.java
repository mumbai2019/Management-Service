//package tests;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeoutException;
//
//import org.json.JSONObject;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import rabbitMQ.Receiver;
//import rabbitMQ.ReceiverTester;
//import rabbitMQ.Sender;
//import rabbitMQ.SenderImplementation;
//import rabbitMQ.EventEnums;
//
//
//
///**
// * Made to testing rabbitMQ.
// * @author s164168 Adam
// */
//public class RabbitMQTest
//{
//
//	static Receiver recv;
//	static ReceiverTester recvReturn;
//	static SenderImplementation sender;
//	
//	/**
//	 * @author s164168 Adam
//	 * @throws IOException 
//	 * @throws TimeoutException 
//	 */
//	@BeforeClass
//	public static void init() throws IOException, TimeoutException
//	{
//		recv = new Receiver();
//		recv.run();
//		
//		recvReturn = new ReceiverTester();
//		recvReturn.run();
//		
//		sender = new SenderImplementation();
//	    
//	}
//	 
//
//	
//	/**
//	 * @author s164168 Adam
//	 * @throws IOException 
//	 * @throws TimeoutException 
//	 */
//	@Test
//	public void findUserByCPR() throws IOException, TimeoutException
//	{
//		
//		JSONObject outgoingJson = new JSONObject();
//		outgoingJson.put("event",EventEnums.USERVERIFYEVENT);
//		outgoingJson.put("cpr", "201397-0071");
//        //Sending message
//        sender.pushMessageTo(outgoingJson.toString(), "manager");
//        
//        
//        try {
//			TimeUnit.SECONDS.sleep(2);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		}
//
//	/**
//	 * @author s164168 Adam
//	 * @throws IOException 
//	 * @throws TimeoutException 
//	 */
//	@Test
//	public void failFindUserByCPR() throws IOException, TimeoutException
//	{
//		
//		JSONObject outgoingJson = new JSONObject();
//		outgoingJson.put("event",EventEnums.USERVERIFYEVENT);
//		outgoingJson.put("cpr", "cpr");
//        //Sending message
//        sender.pushMessageTo(outgoingJson.toString(), "manager");
//
//        
//        try {
//			TimeUnit.SECONDS.sleep(2);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		}
//	
////	@AfterClass
////	public static void cleanup() throws IOException, TimeoutException
////	{
////		recv.close();
////		
////		recvReturn.close();
////		
////		sender.close();
////	    
////	}
//	 
//	
//	
//}
