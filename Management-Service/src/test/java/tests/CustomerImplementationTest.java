package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import customer.Customer;
import customer.CustomerImplementation;
/**
 * Made to testing TokenIDImplementation.
 * @author s164166 Patrick
 * @authro Modified by s164168 Adam to work with the managemen service
 */
public class CustomerImplementationTest
{
	String name = "Patrick";
	String cpr = "229709-1389";
	Customer customer;
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void init()
	{
		customer = new CustomerImplementation(name, cpr);
	}
	 
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void customerGetName()
	{
		String result = customer.getUserName();
		assertEquals(name, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getIDhashcode()
	{
		int expected = 291 + cpr.hashCode();
		int result = customer.getIDhashcode();
		assertEquals(expected, result);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Test
	public void getUserID()
	{
		String result = customer.getUserID().getID();
		assertEquals(cpr, result);
	}
	 
}
