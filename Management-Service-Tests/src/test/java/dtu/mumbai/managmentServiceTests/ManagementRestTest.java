package dtu.mumbai.managmentServiceTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import mumbai.common.restClient.RestClient;


/**
 * Made to testing ManagementRestInterface
 * @author s164168 Adam
 */


public class ManagementRestTest
{
	RestClient rest;
	
	@Before
	public void init() {
	rest = new RestClient("http://02267-mumbai.compute.dtu.dk:4002");
	
	}

	@Test
	public void testRootInterface() {
		Response web = rest.get("/manager",MediaType.TEXT_PLAIN);
		assertEquals("Welcome to the manager rest-interface",web.readEntity(String.class));	
	}
	
	@Test
	public void signUp() {
		JSONObject jsonOut = new JSONObject();
		jsonOut.put("cpr", "123456-7896");
		jsonOut.put("firstName", "Jessie Bondage");
		jsonOut.put("lastName", "Jessie Bondage");
		Response web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
		
		if( web.getStatus() != 200 ){
			fail("TEST ERROR. Error status: " + web.getStatus() + ". Message recived: " + web.readEntity(String.class));
		}
		
		
		assertEquals( new JSONObject().toString(), web.readEntity(String.class));
	}
	
	@Test
	public void signUpDuplicate() {
		JSONObject jsonOut = new JSONObject();
		jsonOut.put("cpr", "123456-7890");
		jsonOut.put("firstName", "Jessie Bondage");
		jsonOut.put("lastName", "Jessie Bondage");
		Response web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
		web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
		JSONObject result;
		if(web.readEntity(String.class) != ""){
			result = new JSONObject(web.readEntity(String.class));
			} else {
			result = new JSONObject();	
			}
		
		if( web.getStatus() != 409 ){
			
			web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
			assertEquals(409,web.getStatus());
			if(web.readEntity(String.class) != ""){
				result = new JSONObject(web.readEntity(String.class));
				} else {
				result = new JSONObject();	
				}
		}
		
			
		
		String error = result.getString("error");
		assertEquals( "User Exists", error);
		
	}
	
//	@Test
//	public void delete() {
//		JSONObject jsonOut = new JSONObject();
//		jsonOut.put("cpr", "123456-7890");
//		Response web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
//		
//		if( web.getStatus() == 400 )
//			System.out.println("TEST ERROR HERE: " + web.readEntity(String.class));
//		System.out.println("RESULT: " + web.readEntity(String.class));
//		
//		JSONObject result = new JSONObject(web.readEntity(String.class));
//			
//		assertEquals( new JSONObject(), result);
//	}
//	
//	@Test
//	public void deleteNonExistent() {
//		JSONObject jsonOut = new JSONObject();
//		jsonOut.put("cpr", "123456-7890");
//		Response web = rest.post("manager/signup", jsonOut.toString(), MediaType.APPLICATION_JSON);
//		
//		if( !(web.getStatus() == 400) )
//			fail("TEST ERROR HERE: " + web.readEntity(String.class));
//		
//		JSONObject result = new JSONObject(web.readEntity(String.class));
//		String error = result.getString("error");
//		assertEquals( "User does not exist", error);
//	}
//	
	
	
}